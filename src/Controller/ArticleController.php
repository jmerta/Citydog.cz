<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Entity\Visitor;
use App\Repository\PostsRepository;
use App\Resolver\ActualMonthResolver;
use App\Resolver\AdsResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController {

    /**
     * @Route("/clanek/detail/{url}", name="article_detail")
     */
    public function index($url, Request $request) {
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));
        $repo = $this->getDoctrine()->getRepository(Posts::class);

        $post = $repo->findOneBy(['url' => $url]);
        $topPosts = $repo->findTopPosts();

        $this->incrementReadCount($post);

        $visitor = null;
        if ($this->getUser()) {
            $visitor = $this->getDoctrine()->getRepository(Visitor::class)->findOneBy(
                ['email' => $this->getUser()->getUsername()]
            );
        }



        return $this->render(
            'article/index.html.twig',
            [
                'showSeznam' => $showSeznam,
                'post' => $post,
                'top' => $topPosts,
                'visitor' => $visitor,
                'controller_name' => 'Detail',
                'shouldIncludeNavbarAd' => true,
                'breadcrumbs' => [
                    ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                    [
                        'name' => $post->getCategory()->getTitle(),
                        'link' => $this->generateUrl('article_category', ['category' => $post->getCategory()->getUrl()])
                    ],
                    ['name' => 'detail článku', 'link' => $this->generateUrl('article_detail', ['url' => $url])]
                ]
            ]
        );
    }

    /**
     * @Route("/clanky/{page}", name="article_list", requirements={"page"="\d+"})
     */
    public function list(int $page = 1, Request $request) {
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        /** @var PostsRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Posts::class);
        $posts = $repo->findForList($page);
        $topPosts = $repo->findTopPosts();
        $topActualMonth = $repo->findTopPostsActualMonth();
        $mainpost = null;

        if ($page == 1 && count($posts)) {
            $mainpost = $posts[0];
            $posts = array_slice($posts, 1);
        }

        $visitor = $this->getUser();

        return $this->render(
            'homepage/articles.html.twig',
            [
                'showSeznam' => $showSeznam,
                'controller_name' => 'Články',
                'topActualMonth' => $topActualMonth,
                'actualMonth' => ActualMonthResolver::resolveActualMonth(),
                'topposts' => $topPosts,
                'posts' => $posts,
                'pagesCount' => $repo->getPageCount(),
                'actualPage' => $page,
                'mainpost' => $mainpost,
                'shouldIncludeNavbarAd' => true,
                'visitor' => $visitor,
                'breadcrumbs' => [
                    ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                    ['name' => 'Články', 'link' => $this->generateUrl('article_list')]
                ]
            ]
        );
    }

    /**
     * @Route("/clanky/kategorie/{category}/{page}", name="article_category", requirements={"page"="\d+"})
     */
    public function category(string $category, int $page = 1, Request $request) {
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        /** @var PostsRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Posts::class);
        $categoryRepo = $this->getDoctrine()->getRepository(Posts\Category::class);

        $posts = $repo->findByCategory($category, $page);
        $topPosts = $repo->findTopPosts();
        $topActualMonth = $repo->findTopPostsActualMonth();
        $mainpost = null;

        if ($page == 1 && count($posts)) {
            $mainpost = $posts[0];
            $posts = array_slice($posts, 1);
        }

        /** @var $categoryObj Posts\Category */
        $categoryObj =  $categoryRepo->findOneBy(['url' => $category]);

        return $this->render(
            'article/category.html.twig',
            [
                'showSeznam' => $showSeznam,
                'controller_name' => 'Kategorie',
                'category' => $categoryObj,
                'topActualMonth' => $topActualMonth,
                'actualMonth' => ActualMonthResolver::resolveActualMonth(),
                'topposts' => $topPosts,
                'posts' => $posts,
                'pagesCount' => $repo->getPageCountByCategory($category),
                'actualPage' => $page,
                'mainpost' => $mainpost,
                'shouldIncludeNavbarAd' => true,
                'breadcrumbs' => [
                    ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                    ['name' =>  $categoryObj->getTitle(), 'link' => $this->generateUrl('article_category', ['category' => $category])]
                ]
            ]
        );
    }

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'adsResolver' => AdsResolver::class,
            'enviroment' => KernelInterface::class
        ]);
    }

    private function incrementReadCount(?Posts $post): void {
        /** @var  KernelInterface $kernel */
        $kernel = $this->get('enviroment');

        if ($kernel->getEnvironment() === 'prod') {
            $manager = $this->getDoctrine()->getManager();
            $viewCount = $post->getViewCount() + 1;
            $post->setViewCount($viewCount);
            $manager->flush();
        }
    }
}
