<?php declare(strict_types=1);


namespace App\Controller;


use App\Entity\Comment;
use App\Entity\Posts;
use App\Entity\Visitor;
use App\Factory\CommentFactory;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController {

    /**
     * @param Request $request
     * @Route("/odeslat-komentar", name="send_comment")
     * @return Response
     */
    public function CommentAction(Request $request): Response {
        $name = $request->get('name');
        $text = $request->get('textComment');
        $email = $request->get('email');
        $postId = $request->get('postId');
        $visitorId = $request->get('visitorId');
        $parentCommentId = $request->get('parentCommentId');

        if ($request->get('nick') != "") {
            return $this->redirect('https://securelist.com/redirects-in-spam/57924/');
        }

        /** @var Posts $post */
        $post = $this->getDoctrine()->getRepository(Posts::class)->find($postId);
        $parentComment = $parentCommentId ? $this->getDoctrine()->getRepository(Comment::class)->find($parentCommentId) : null;

        $visitor = null;
        if ($visitorId) {
            $visitor = $this->getDoctrine()->getRepository(Visitor::class)->find($visitorId);
        }

        $comment = CommentFactory::create($text, $post, $parentComment, $visitor, $name, $email);
        $em = $this->getDoctrine()->getManagerForClass(Comment::class);
        $em->persist($comment);
        $em->flush();

        return $this->redirectToRoute('article_detail', ['url' => $post->getUrl()]);
    }

    /**
     * @param Request $request
     * @Route("/prihlaseny-uzivatel", name="logged_user_fuck")
     * @return Response
     */
    public function loggedUserAction(Request $request): Response {
        $visitor = null;
        if ($this->getUser()) {
            $visitor = $this->getDoctrine()->getRepository(Visitor::class)->findOneBy(
                ['email' => $this->getUser()->getUsername()]
            );
        }

        return new JsonResponse([$visitor, $this->getUser()]);
    }
}
