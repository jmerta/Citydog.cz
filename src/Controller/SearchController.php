<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Repository\PostsRepository;
use App\Resolver\AdsResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{

    /**
     * @Route("/vyhledavac", name="search")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $phrase = null;
        $posts = null;
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        if ($request->isMethod('POST')) {
            $phrase = $request->get('search_phrase');

            /** @var PostsRepository $repo */
            $repo = $this->getDoctrine()->getRepository(Posts::class);
            $posts = $repo->searchByPhrase($phrase);
        }

        return $this->render('search/index.html.twig', [
            'showSeznam' => $showSeznam,
            'controller_name' => 'Vyhledávač',
            'shouldIncludeNavbarAd' => false,
            'phrase' => $phrase,
            'posts' => $posts,
            'breadcrumbs' => [
                ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                ['name' => 'vyhledávač', 'link' => $this->generateUrl('search')]
            ]
        ]);
    }

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'adsResolver' => AdsResolver::class
        ]);
    }
}
