<?php declare(strict_types=1);


namespace App\Controller;

use App\Resolver\AdsResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StreetFoodController extends AbstractController {

    /**
     * @Route("/street-food", name="street_food")
     */
    public function index(Request $request) {
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        return $this->render('streetfood/index.html.twig', [
            'showSeznam' => $showSeznam,
            'controller_name' => 'Streetfood mapa',
            'shouldIncludeNavbarAd' => false,
            'breadcrumbs' => [
                ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                ['name' => 'streetfood mapa', 'link' => $this->generateUrl('street_food')]
            ]
        ]);

    }

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'adsResolver' => AdsResolver::class
        ]);
    }
}
