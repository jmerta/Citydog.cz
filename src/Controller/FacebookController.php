<?php declare(strict_types=1);


namespace App\Controller;


use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class FacebookController extends AbstractController {

    const REDIRECT_URI_FB = 'fb_redirect_uri';
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Link to this controller to start the "connect" process
     * @param Request        $request
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     * @Route("/connect/facebook", name="connect_facebook_start")
     *
     */
    public function connectAction(Request $request, ClientRegistry $clientRegistry)
    {

        if ($request->get('redirect_uri')) {
            $this->session->set(self::REDIRECT_URI_FB, $request->get('redirect_uri'));
        }

        return $clientRegistry
            ->getClient('facebook')
            ->redirect([
                'public_profile', 'email' // the scopes you want to access
            ]
           )
            ;
    }

    /**
     * After going to Facebook, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @return RedirectResponse
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        if ($this->session->get(self::REDIRECT_URI_FB)) {
            return $this->redirectToRoute('article_detail', ['url' => $this->session->get(self::REDIRECT_URI_FB)]);

        }

        return $this->redirectToRoute('homepage');

    }

}
