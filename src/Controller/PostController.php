<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/post/{url}", name="post_detail")
     */
    public function index(string $url)
    {
        return $this->redirectToRoute('article_detail', ['url' => $url], 301);
    }
}
