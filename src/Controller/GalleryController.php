<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Resolver\AdsResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    /**
     * @Route("/galerie/{url}/{page}", name="gallery", requirements={"page"="\d+"})
     */
    public function index(string $url, int $page = 1, Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Posts::class);

        /** @var Posts $post */
        $post = $repo->findOneBy(['url' => $url]);

        if (!$post->hasGallery()) {
            return $this->redirectToRoute('article_detail', ['url' => $post->getUrl()]);
        }

        $image = $post->getImages()->get(($page-1));

        if (!$image) {
            return $this->redirectToRoute('article_detail', ['url' => $post->getUrl()]);
        }

        $count = $post->getGalleryImageCount();
        $source =  $post->getGallerySource();
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        return $this->render('gallery/index.html.twig', [
            'showSeznam' => $showSeznam,
        'controller_name' => 'Galerie',
            'shouldIncludeNavbarAd' => false,
            'page' => $page,
            'image' => $image,
            'count' => $count,
            'url' => $post->getUrl(),
            'source' => $source,
            'breadcrumbs' => []
        ]);
    }

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'adsResolver' => AdsResolver::class
        ]);
    }
}
