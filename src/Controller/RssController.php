<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Posts;
use App\Repository\PostsRepository;
use Eko\FeedBundle\Feed\Feed;
use Eko\FeedBundle\Feed\FeedManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RssController extends AbstractController {


    /**
     * Generate the article feed
     *
     * @Route("/rss/citydog.rss", name="app_feed")
     *
     * @return Response XML Feed
     */
    public function feed()
    {
        /** @var $repo PostsRepository */
        $repo = $this->getDoctrine()->getRepository(Posts::class);
        $posts = $repo->findForHomepage();

        /** @var $feed Feed*/
        $feed = $this->get('feed_manager')->get('article');
        $feed->addFromArray($posts);

        $response =  new Response($feed->render('rss'));
        $response->headers->set('Content-Type', 'application/rss+xml');

        return $response;
    }



    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'feed_manager' => FeedManager::class
        ]);
    }
}
