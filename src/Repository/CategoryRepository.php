<?php declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;


class CategoryRepository extends EntityRepository {

    public static function activeCategories(CategoryRepository $repository): QueryBuilder {
        return $repository->createQueryBuilder('c')
            ->where("c.active IS TRUE")
            ->orderBy('c.title');
    }
}
