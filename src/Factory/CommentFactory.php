<?php declare(strict_types=1);


namespace App\Factory;


use App\Entity\Comment;
use App\Entity\Posts;
use App\Entity\Visitor;
use DateTime;

class CommentFactory {

    public static function create(
        string $text,
        Posts $post,
        ?Comment $parentComment,
        ?Visitor $visitor,
        ?string $name,
        ?string $email
    ): Comment {
        $comment = new Comment();

        $comment->setApproved(false);
        $comment->setCreateDate(new DateTime());
        $comment->setStatusChangeDate(new DateTime());
        $comment->setComment($text);
        $comment->setCommentClear($text);
        $comment->setVisitor($visitor);
        if ($visitor) {
            $email = $visitor->getEmail();
            $name = $visitor->getFullname();
            $comment->setApproved(true);
        }
        $comment->setEmail($email);
        $comment->setName($name);
        $comment->setPost($post);
        $comment->setMd5(md5($text));
        $comment->setParent($parentComment);

        return $comment;
    }
}
