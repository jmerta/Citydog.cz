<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgCommentsBanlist
 *
 * @ORM\Table(name="ctg_comments_banlist", indexes={@ORM\Index(name="id_item_2", columns={"id_visitor"}), @ORM\Index(name="create_date", columns={"created_at"}), @ORM\Index(name="id_visitor", columns={"id_visitor"})})
 * @ORM\Entity
 */
class CtgCommentsBanlist
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=255, nullable=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=24, nullable=false)
     */
    private $ip = '';

    /**
     * @var array
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=false, options={"default"="WAIT"})
     */
    private $status = 'WAIT';

    /**
     * @var string
     *
     * @ORM\Column(name="status_note", type="string", length=100, nullable=false)
     */
    private $statusNote;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="status_change_date", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $statusChangeDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="status_changed_by", type="string", length=30, nullable=false)
     */
    private $statusChangedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="banned_till", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $bannedTill = '0000-00-00 00:00:00';

    /**
     * @var int
     *
     * @ORM\Column(name="id_visitor", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idVisitor = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ga_uid", type="text", length=65535, nullable=true)
     */
    private $gaUid;

    /**
     * @var int
     *
     * @ORM\Column(name="ym_uid", type="bigint", nullable=false)
     */
    private $ymUid;

    /**
     * @var int
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false)
     */
    private $idComment;


}
