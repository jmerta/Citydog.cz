<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgChannels
 *
 * @ORM\Table(name="ctg_channels", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"url"})})
 * @ORM\Entity
 */
class CtgChannels
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=20, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="is_default", type="integer", nullable=false)
     */
    private $isDefault = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=128, nullable=false)
     */
    private $shortName = '';

    /**
     * @var int
     *
     * @ORM\Column(name="show_in_adminka", type="integer", nullable=false)
     */
    private $showInAdminka = '0';


}
