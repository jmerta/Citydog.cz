<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPlace
 *
 * @ORM\Table(name="ctg_place", indexes={@ORM\Index(name="place_name", columns={"place_name"})})
 * @ORM\Entity
 */
class CtgPlace
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_place", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="place_name", type="string", length=128, nullable=false)
     */
    private $placeName;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=128, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=128, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="text", length=65535, nullable=false)
     */
    private $postalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="area", type="string", length=128, nullable=true)
     */
    private $area;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street2", type="string", length=128, nullable=true)
     */
    private $street2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="string", length=128, nullable=true)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="house1", type="string", length=20, nullable=true)
     */
    private $house1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="house", type="string", length=20, nullable=true)
     */
    private $house;

    /**
     * @var string|null
     *
     * @ORM\Column(name="housing", type="string", length=20, nullable=true)
     */
    private $housing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address_info", type="string", length=255, nullable=true)
     */
    private $addressInfo;

    /**
     * @var int
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="properties", type="string", length=20, nullable=false)
     */
    private $properties;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coord_lat", type="string", length=20, nullable=true)
     */
    private $coordLat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coord_long", type="string", length=20, nullable=true)
     */
    private $coordLong;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    private $params;


}
