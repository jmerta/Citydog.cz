<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgBrandingItems
 *
 * @ORM\Table(name="ctg_branding_items", indexes={@ORM\Index(name="id_item", columns={"id_item"})})
 * @ORM\Entity
 */
class CtgBrandingItems
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     */
    private $idItem;

    /**
     * @var int
     *
     * @ORM\Column(name="id_branding", type="integer", nullable=false)
     */
    private $idBranding;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false, options={"default"="1"})
     */
    private $status = true;


}
