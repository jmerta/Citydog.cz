<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPlaceProperties
 *
 * @ORM\Table(name="ctg_place_properties")
 * @ORM\Entity
 */
class CtgPlaceProperties
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_properties", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProperties;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;


}
