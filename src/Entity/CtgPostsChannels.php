<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostsChannels
 *
 * @ORM\Table(name="ctg_posts_channels", uniqueConstraints={@ORM\UniqueConstraint(name="id_post", columns={"id_post", "id_channel"})})
 * @ORM\Entity
 */
class CtgPostsChannels
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idPost;

    /**
     * @var int
     *
     * @ORM\Column(name="id_channel", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="id_channel_text", type="string", length=20, nullable=false)
     */
    private $idChannelText;


}
