<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatPostView
 *
 * @ORM\Table(name="ctg_stat_post_view", uniqueConstraints={@ORM\UniqueConstraint(name="id_post", columns={"id_post"})})
 * @ORM\Entity
 */
class CtgStatPostView
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idPost;

    /**
     * @var int
     *
     * @ORM\Column(name="last_10min", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last10min = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="last_1hour", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last1hour = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="prev_1hour", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $prev1hour = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="last_2hours", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last2hours = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="last_8hours", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last8hours = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="last_24hours", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last24hours = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="last_18hours", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last18hours;

    /**
     * @var int
     *
     * @ORM\Column(name="last_7days", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $last7days;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $total = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;


}
