<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgSitePages1
 *
 * @ORM\Table(name="ctg_site_pages1", indexes={@ORM\Index(name="sequence", columns={"sequence"}), @ORM\Index(name="url", columns={"url"})})
 * @ORM\Entity
 */
class CtgSitePages1
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_page", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var int
     *
     * @ORM\Column(name="sequence", type="integer", nullable=false)
     */
    private $sequence;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="id_template", type="integer", nullable=false)
     */
    private $idTemplate;

    /**
     * @var int
     *
     * @ORM\Column(name="id_perm", type="integer", nullable=false, options={"default"="1"})
     */
    private $idPerm = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="show_in_map", type="boolean", nullable=false)
     */
    private $showInMap = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="show_in_menu", type="boolean", nullable=false)
     */
    private $showInMenu = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="title_menu", type="string", length=255, nullable=false)
     */
    private $titleMenu = '';

    /**
     * @var int
     *
     * @ORM\Column(name="id_parent", type="integer", nullable=false)
     */
    private $idParent = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="undeletable", type="integer", nullable=true)
     */
    private $undeletable = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true, options={"default"="1"})
     */
    private $isActive = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="id_structure", type="boolean", nullable=false)
     */
    private $idStructure = '0';


}
