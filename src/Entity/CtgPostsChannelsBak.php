<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostsChannelsBak
 *
 * @ORM\Table(name="ctg_posts_channels_bak", uniqueConstraints={@ORM\UniqueConstraint(name="id_post", columns={"id_post", "id_channel"})})
 * @ORM\Entity
 */
class CtgPostsChannelsBak
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idPost;

    /**
     * @var int
     *
     * @ORM\Column(name="id_channel", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="id_channel_text", type="string", length=20, nullable=false)
     */
    private $idChannelText;


}
