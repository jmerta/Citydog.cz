<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostsCategories
 *
 * @ORM\Table(name="ctg_posts_categories", indexes={@ORM\Index(name="id_post", columns={"id_post", "id_category"}), @ORM\Index(name="id_category", columns={"id_category", "id_post"})})
 * @ORM\Entity
 */
class CtgPostsCategories
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     */
    private $idPost = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false)
     */
    private $idCategory = '0';


}
