<?php

namespace App\Entity;

use App\Entity\Posts\Category;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Eko\FeedBundle\Item\Writer\RoutedItemInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(
 *     name="ctg_posts",
 *     indexes={@ORM\Index(name="status", columns={"status"}),
 *              @ORM\Index(name="sort_date", columns={"sort_date"}),
 *              @ORM\Index(name="ctg_posts_idx_statu_type_user_post_date_comme", columns={"status", "feed_type", "id_user", "id_post", "sort_date", "approved_comments"}),
 *              @ORM\Index(name="url", columns={"url"}), @ORM\Index(name="lang", columns={"lang"}),
 *              @ORM\Index(name="ctg_posts_idx_date_post", columns={"sort_date", "id_post"}),
 *              @ORM\Index(name="id_user", columns={"id_user"}),
 *              @ORM\Index(name="sort_date_2", columns={"sort_date", "id_post"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\PostsRepository")
 * @Vich\Uploadable
 */
class Posts implements RoutedItemInterface, WebfixInterface
{

    const DEFAULT_TEXT = '<p><strong>Nadpis</strong></p>'
    .'<p >text článku</p><p ></p>'
    .'<p style="text-align: center;">&nbsp; <img src="https://city-dog.cz/images/dog_step.png" style="height:14px; vertical-align:text-top; margin:1px 1px 0;" />&nbsp;<strong>Fotografie:</strong>&nbsp;city-dog.cz.</p>';

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idPost;

    /**
     * @var User
     * @ManyToOne(targetEntity="App\Entity\User")
     * @JoinColumn(name="id_user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var User
     * @ManyToOne(targetEntity="App\Entity\User")
     * @JoinColumn(name="id_author_text", referencedColumnName="id")
     */
    private $author;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="page_title", type="string", length=255, nullable=false)
     */
    private $pageTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=false)
     */
    private $keywords = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="post_title", type="string", length=255, nullable=false)
     */
    private $postTitle = '';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="post_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $postDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="sort_date2", type="date", nullable=false, options={"default"="0000-00-00"})
     */
    private $sortDate2;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="sort_date", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $sortDate;

    /**
     * @var string
     *
     * @ORM\Column(name="post_image", type="string", length=255, nullable=false)
     */
    private $postImage = '';

    /**
     * @Vich\UploadableField(mapping="post_images", fileNameProperty="postImage")
     * @Assert\File(maxSize="3M", maxSizeMessage="Maximální velikost obrázku je 3MB")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_summary", type="text", length=65535, nullable=true)
     */
    private $summary;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_description", type="text", length=65535, nullable=true)
     */
    private $postDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=16777215, nullable=true)
     */
    private $text = self::DEFAULT_TEXT;

    /**
     * @var ArrayCollection
     * @ManyToMany(targetEntity="App\Entity\Posts\Category")
     * @JoinTable(name="ctg_posts_categories",
     *      joinColumns={@JoinColumn(name="id_post", referencedColumnName="id_post")},
     *      inverseJoinColumns={@JoinColumn(name="id_category", referencedColumnName="id_category")}
     *      )
     */
    private $categories;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post")
     */
    private $comments;

    /**
     * @var array|null
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=true)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="feed_type", type="integer", nullable=false, options={"default"="1"})
     */
    private $feedType = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="exclude_from_main", type="integer", nullable=false)
     */
    private $excludeFromMain = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="not_in_rss", type="boolean", nullable=true)
     */
    private $notInRss = false;

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="block_comments", type="boolean", nullable=false)
     */
    private $blockComments = false;

    /**
     * @var int
     *
     * @ORM\Column(name="approved_comments", type="integer", nullable=false)
     */
    private $approvedComments = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=3, nullable=false, options={"default"="ru"})
     */
    private $lang = 'cs';

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=false)
     */
    private $params = '';

    /**
     * @var string|null
     * @ORM\Column(name="author_text", type="string", length=200, nullable=true)
     */
    private $authorText;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $gallerySource;

    /**
     * @var GalleryImage[]|ArrayCollection
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="App\Entity\GalleryImage", mappedBy="posts", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"imageOrder" = "ASC"})
     */
    private $images;

    public function __construct() {
        $this->categories = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->status[0] = 'DRAFT';
        $this->postDate = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getIdPost(): ?int {
        return $this->idPost;
    }

    /**
     * @param int $idPost
     */
    public function setIdPost(int $idPost): void {
        $this->idPost = $idPost;
    }
    /**
     * @return Collection
     */
    public function getCategories(): Collection {
        return $this->categories;
    }

    /**
     * @param ArrayCollection $categories
     */
    public function setCategories(ArrayCollection $categories): void {
        $this->categories = $categories;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category {
        return $this->categories->first() ?: null;
    }

    /**
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void {
        $this->categories->clear();
        $this->categories->add($category);
    }

    /**
     * @return User
     */
    public function getUser(): User {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void {
        $this->author = $author;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url): void {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getPageTitle(): string {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     */
    public function setPageTitle(string $pageTitle): void {
        $this->pageTitle = $pageTitle;
    }

    /**
     * @return string
     */
    public function getKeywords(): string {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords(string $keywords): void {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPostTitle(): string {
        //$postTitle = utf8_encode($this->postTitle);
        $postTitle = str_replace('</p>', '<br>', $this->postTitle);
        $postTitle = str_replace('<p>', '', $postTitle);
        $postTitle = html_entity_decode($postTitle);
        $postTitle = htmlspecialchars_decode($postTitle);

        return $postTitle;
    }

    public function getTitleRaw() {
        return $this->postTitle;
    }

    /**
     * @param string $postTitle
     */
    public function setPostTitle(string $postTitle): void {
        $this->postTitle = $postTitle;
    }

    /**
     * @return DateTime|null
     */
    public function getPostDate(): ?DateTime {
        return $this->postDate;
    }

    /**
     * @param DateTime $postDate
     */
    public function setPostDate(DateTime $postDate): void {
        $this->postDate = $postDate;
        $this->sortDate = $postDate;
        $this->sortDate2 = $postDate;
    }

    /**
     * @return DateTime
     */
    public function getSortDate2(): DateTime {
        return $this->sortDate2;
    }

    /**
     * @param DateTime $sortDate2
     */
    public function setSortDate2(DateTime $sortDate2): void {
        $this->sortDate2 = $sortDate2;
    }

    /**
     * @return DateTime
     */
    public function getSortDate(): DateTime {
        return $this->sortDate;
    }

    /**
     * @param DateTime $sortDate
     */
    public function setSortDate(DateTime $sortDate): void {
        $this->sortDate = $sortDate;
    }

    /**
     * @return string
     */
    public function getPostImage(): string {
        return $this->postImage;
    }

    public function getImageUrl() {
        return 'https://city-dog.cz/content/_posts/'.$this->postImage;
    }

    /**
     * @param string|null $postImage
     */
    public function setPostImage(?string $postImage): void {
        $this->postImage = $postImage;
    }

    /**
     * @return string|null
     */
    public function getSummary(): ?string {
        return $this->summary;
    }

    /**
     * @param string|null $summary
     */
    public function setSummary(?string $summary): void {
        $this->summary = $summary;
    }

    /**
     * @return string|null
     */
    public function getPostDescription(): ?string {
        return $this->postDescription;
    }

    /**
     * @param string|null $postDescription
     */
    public function setPostDescription(?string $postDescription): void {
        $this->postDescription = $postDescription;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void {
        $this->text = $text;
    }

    /**
     * @return array|null
     */
    public function getStatus(): ?array {
        return $this->status;
    }

    /**
     * @param array|null $status
     */
    public function setStatus(?array $status): void {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getFeedType(): int {
        return $this->feedType;
    }

    /**
     * @param int $feedType
     */
    public function setFeedType(int $feedType): void {
        $this->feedType = $feedType;
    }

    /**
     * @return int
     */
    public function getExcludeFromMain(): int {
        return $this->excludeFromMain;
    }

    /**
     * @param int $excludeFromMain
     */
    public function setExcludeFromMain(int $excludeFromMain): void {
        $this->excludeFromMain = $excludeFromMain;
    }

    /**
     * @return bool|null
     */
    public function getNotInRss(): ?bool {
        return $this->notInRss;
    }

    /**
     * @param bool|null $notInRss
     */
    public function setNotInRss(?bool $notInRss): void {
        $this->notInRss = $notInRss;
    }

    /**
     * @return int
     */
    public function getViewCount(): int {
        return $this->viewCount;
    }

    /**
     * @param int $viewCount
     */
    public function setViewCount(int $viewCount): void {
        $this->viewCount = $viewCount;
    }

    /**
     * @return bool
     */
    public function isBlockComments(): bool {
        return $this->blockComments;
    }

    /**
     * @param bool $blockComments
     */
    public function setBlockComments(bool $blockComments): void {
        $this->blockComments = $blockComments;
    }

    /**
     * @return int
     */
    public function getApprovedComments(): int {
        return $this->approvedComments;
    }

    /**
     * @param int $approvedComments
     */
    public function setApprovedComments(int $approvedComments): void {
        $this->approvedComments = $approvedComments;
    }

    /**
     * @return string
     */
    public function getLang(): string {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getParams(): string {
        return $this->params;
    }

    /**
     * @param string $params
     */
    public function setParams(string $params): void {
        $this->params = $params;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection {
        $criteria = Criteria::create();

        $criteria->andWhere(Criteria::expr()->eq('status', 'APPROVE'));
        $criteria->andWhere(
            Criteria::expr()->orX(
                Criteria::expr()->isNull('parent')
            )
        );


        return $this->comments->matching($criteria);
    }

    public function getCommentsCount() {
        $count = 0;
        foreach ($this->getComments() as $comment) {
            $count += $this->countComments($comment);
        }

        return $count;
    }

    private function countComments(Comment $comment): int {

        if ($comment->getChildren()->isEmpty()) {
            return 1;
        }
            $count = 0;
            foreach($comment->getChildren() as $child) {
                $count += $this->countComments($child);
            }

            return $count;
    }

    /**
     * @param ArrayCollection $comments
     */
    public function setComments(ArrayCollection $comments): void {
        $this->comments = $comments;
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemTitle() {
        return $this->getPostTitle();
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemDescription() {
        return $this->getSummary();
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemLink() {
        return $this->getUrl();
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemPubDate() {
        return $this->getPostDate();
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemRouteName() {
        return 'article_detail';
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemRouteParameters() {
        return ['url' => $this->getUrl()];
    }

    /**
     * @inheritDoc
     */
    public function getFeedItemUrlAnchor() {
        return '';
    }

    public function __toString() {
        return (string)$this->idPost;
    }

    /**
     * @return bool
     */
    public function getPublished(): bool {
        return $this->status[0] === 'PUBLISH';
    }

    /**
     * @param bool $published
     */
    public function setPublished(bool $published): void {
        $this->status[0] = 'DRAFT';

        if ($published) {
            $this->status[0] = 'PUBLISH';
        }
    }

    /**
     * @return string|null
     */
    public function getAuthorText(): ?string {
        if (!$this->authorText) {
            return $this->author ? $this->author->getDisplayName() : null;
        }
        return $this->authorText;
    }

    /**
     * @param string|null $authorText
     */
    public function setAuthorText(?string $authorText): void {
        $this->authorText = $authorText;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setUpdatedAt(?DateTime $updatedAt): void {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Get images
     *
     * @return GalleryImage[]|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param GalleryImage[]|ArrayCollection $images
     */
    public function setImages($images): void {
        foreach ($images as $image) {
            $this->addImage($image);
        }
    }

    /**
     * Add image
     *
     * @param GalleryImage $image
     *
     */
    public function addImage(GalleryImage $image)
    {
        $image->setPosts($this);
        $this->images[] = $image;
        $this->updatedAt = new DateTime('now');
    }

    public function hasGallery(): bool {
        return !$this->images->isEmpty();
    }

    public function getGalleryImageCount(): int {
        return $this->images->count();
    }

    /**
     * Remove image
     *
     * @param GalleryImage $image
     */
    public function removeImage(GalleryImage $image)
    {
        $this->images->removeElement($image);
        $image->setPosts(null);
    }

    /**
     * @return string|null
     */
    public function getGallerySource(): ?string {
        return $this->gallerySource;
    }

    /**
     * @param string|null $gallerySource
     */
    public function setGallerySource(?string $gallerySource): void {
        $this->gallerySource = $gallerySource;
    }

    public function getTitleClear() {
        $postTitle = html_entity_decode($this->postTitle);
        $postTitle = htmlspecialchars_decode($postTitle);

        return strip_tags($postTitle);
    }
}
