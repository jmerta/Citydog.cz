<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgRaspisanie
 *
 * @ORM\Table(name="ctg_raspisanie", indexes={@ORM\Index(name="date", columns={"date"}), @ORM\Index(name="id_event_3", columns={"id_event", "date_end", "date"}), @ORM\Index(name="date_end", columns={"date_end"})})
 * @ORM\Entity
 */
class CtgRaspisanie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_raspisanie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRaspisanie;

    /**
     * @var int
     *
     * @ORM\Column(name="id_event", type="integer", nullable=false)
     */
    private $idEvent = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date", nullable=false)
     */
    private $dateEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="time", nullable=false)
     */
    private $time;

    /**
     * @var int
     *
     * @ORM\Column(name="cena_bak", type="integer", nullable=false)
     */
    private $cenaBak;

    /**
     * @var float
     *
     * @ORM\Column(name="cena", type="float", precision=10, scale=0, nullable=false)
     */
    private $cena = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_place", type="integer", nullable=false, options={"default"="1"})
     */
    private $idPlace = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    private $type;


}
