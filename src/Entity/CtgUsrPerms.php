<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgUsrPerms
 *
 * @ORM\Table(name="ctg_usr_perms")
 * @ORM\Entity
 */
class CtgUsrPerms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_perm", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPerm;

    /**
     * @var string
     *
     * @ORM\Column(name="system", type="string", length=100, nullable=false)
     */
    private $system = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';


}
