<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="ctg_usr_users", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements WebfixInterface, UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255, nullable=false)
     */
    private $displayName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="password_old", type="string", length=32, nullable=false)
     */
    private $passwordOld = '';

    /**
     * @var int
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     */
    private $idGroup = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=false)
     */
    private $photo = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="remember_token", type="string", length=100, nullable=true)
     */
    private $rememberToken;

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    private $params;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="acl", type="string", length=20, nullable=false)
     */
    private $acl = 'ROLE_REDACTOR';

    private $roles = [];

    /**
     * @return int
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void {
        $this->id = $id;
        $this->idUser = $id;
    }

    /**
     * @return int
     */
    public function getIdUser(): ?int {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser(int $idUser): void {
        $this->idUser = $idUser;
    }

    /**
     * @return string
     */
    public function getUsername(): string {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
        $this->displayName = $name;
    }

    /**
     * @return string
     */
    public function getDisplayName(): ?string {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName(?string $displayName): void {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPasswordOld(): string {
        return $this->passwordOld;
    }

    /**
     * @param string $passwordOld
     */
    public function setPasswordOld(string $passwordOld): void {
        $this->passwordOld = $passwordOld;
    }

    /**
     * @return int
     */
    public function getIdGroup(): int {
        return $this->idGroup;
    }

    /**
     * @param int $idGroup
     */
    public function setIdGroup(int $idGroup): void {
        $this->idGroup = $idGroup;
    }

    /**
     * @return string
     */
    public function getPhoto(): string {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void {
        $this->photo = $photo;
    }

    /**
     * @return int|null
     */
    public function getIsActive(): ?int {
        return $this->isActive;
    }

    /**
     * @param int|null $isActive
     */
    public function setIsActive(?int $isActive): void {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getRememberToken(): ?string {
        return $this->rememberToken;
    }

    /**
     * @param string|null $rememberToken
     */
    public function setRememberToken(?string $rememberToken): void {
        $this->rememberToken = $rememberToken;
    }

    /**
     * @return string|null
     */
    public function getParams(): ?string {
        return $this->params;
    }

    /**
     * @param string|null $params
     */
    public function setParams(?string $params): void {
        $this->params = $params;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     */
    public function setUpdatedAt(?DateTime $updatedAt): void {
        $this->updatedAt = $updatedAt;
    }

    public function __toString() {
        return $this->displayName;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = $this->acl;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getSalt() {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getAcl(): string {
        return $this->acl;
    }

    /**
     * @param string $acl
     */
    public function setAcl(string $acl): void {
        $this->acl = $acl;
    }

    public function getAclName() {
        if ($this->acl == 'ROLE_REDACTOR') {
            return 'Redaktor';
        }

        return 'Admin';
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword): void {
        $this->plainPassword = $plainPassword;
    }
}
