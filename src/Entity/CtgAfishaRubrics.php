<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgAfishaRubrics
 *
 * @ORM\Table(name="ctg_afisha_rubrics")
 * @ORM\Entity
 */
class CtgAfishaRubrics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRubric;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';


}
