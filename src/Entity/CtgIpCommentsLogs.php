<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgIpCommentsLogs
 *
 * @ORM\Table(name="ctg_ip_comments_logs", indexes={@ORM\Index(name="id_comment", columns={"id_comment"})})
 * @ORM\Entity
 */
class CtgIpCommentsLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLog;

    /**
     * @var int
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false)
     */
    private $idComment = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="ip_int", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $ipInt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createDate = 'CURRENT_TIMESTAMP';


}
