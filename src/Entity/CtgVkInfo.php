<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgVkInfo
 *
 * @ORM\Table(name="ctg_vk_info")
 * @ORM\Entity
 */
class CtgVkInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInfo;

    /**
     * @var int
     *
     * @ORM\Column(name="id_vk", type="integer", nullable=false)
     */
    private $idVk = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string|null
     *
     * @ORM\Column(name="screen_name", type="string", length=255, nullable=true)
     */
    private $screenName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userimage", type="string", length=255, nullable=true)
     */
    private $userimage;

    /**
     * @var string
     *
     * @ORM\Column(name="usercookie", type="string", length=41, nullable=false)
     */
    private $usercookie = '';


}
