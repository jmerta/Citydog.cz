<?php

namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * CtgVisitors
 *
 * @ORM\Table(name="ctg_visitors", indexes={@ORM\Index(name="id_external", columns={"id_external"}), @ORM\Index(name="domain", columns={"domain"})})
 * @ORM\Entity
 */
class Visitor implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_visitor", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=0, nullable=false)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="id_external", type="string", length=50, nullable=false)
     */
    private $idExternal;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=64, nullable=false)
     */
    private $login = '';

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=64, nullable=false)
     */
    private $firstName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=64, nullable=false)
     */
    private $lastName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=500, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="photo_url", type="text", length=65535, nullable=true)
     */
    private $photoUrl;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_registered", type="datetime", nullable=true)
     */
    private $dateRegistered;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_logged", type="datetime", nullable=true)
     */
    private $dateLogged;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_token", type="string", length=255, nullable=true)
     */
    private $lastToken = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="banned", type="boolean", nullable=false)
     */
    private $banned = '0';

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDomain(): string {
        return $this->domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain(string $domain): void {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    public function getIdExternal(): string {
        return $this->idExternal;
    }

    /**
     * @param string $idExternal
     */
    public function setIdExternal(string $idExternal): void {
        $this->idExternal = $idExternal;
    }

    /**
     * @return string
     */
    public function getLogin(): string {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getFirstName(): string {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPhotoUrl(): ?string {
        return $this->photoUrl;
    }

    /**
     * @param string|null $photoUrl
     */
    public function setPhotoUrl(?string $photoUrl): void {
        $this->photoUrl = $photoUrl;
    }

    /**
     * @return DateTime|null
     */
    public function getDateRegistered(): ?DateTime {
        return $this->dateRegistered;
    }

    /**
     * @param DateTime|null $dateRegistered
     */
    public function setDateRegistered(?DateTime $dateRegistered): void {
        $this->dateRegistered = $dateRegistered;
    }

    /**
     * @return DateTime|null
     */
    public function getDateLogged(): ?DateTime {
        return $this->dateLogged;
    }

    /**
     * @param DateTime|null $dateLogged
     */
    public function setDateLogged(?DateTime $dateLogged): void {
        $this->dateLogged = $dateLogged;
    }

    /**
     * @return string|null
     */
    public function getLastToken(): ?string {
        return $this->lastToken;
    }

    /**
     * @param string|null $lastToken
     */
    public function setLastToken(?string $lastToken): void {
        $this->lastToken = $lastToken;
    }

    /**
     * @return bool
     */
    public function isBanned(): bool {
        return $this->banned;
    }

    /**
     * @param bool $banned
     */
    public function setBanned(bool $banned): void {
        $this->banned = $banned;
    }

    public function getRoles() {
        return [];
    }

    public function getPassword() {
        return '';
    }

    public function getSalt() {
        // TODO: Implement getSalt() method.
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }

    public function getFullname() {
        return $this->getFirstName().' '.$this->getLastName();
    }

    public function isFromFacebook() {
        return $this->domain == 'facebook';
    }

    public function getProfileUrl() {
        if ($this->isFromFacebook() && $this->getIdExternal()) {
            return 'https://www.facebook.com/profile.php?id='.$this->getIdExternal();
        } elseif($this->getIdExternal()) {
            return '';
        }
    }
}
