<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * PermissionRoles
 *
 * @ORM\Table(name="permission_roles", indexes={@ORM\Index(name="permission_roles_role_id_foreign", columns={"role_id"})})
 * @ORM\Entity
 */
class PermissionRoles
{
    /**
     * @var int
     *
     * @ORM\Column(name="permission_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $permissionId;

    /**
     * @var int
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $roleId;


}
