<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Revisions
 *
 * @ORM\Table(name="revisions", indexes={@ORM\Index(name="revisions_revisionable_id_revisionable_type_index", columns={"revisionable_id", "revisionable_type"})})
 * @ORM\Entity
 */
class Revisions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="revisionable_type", type="string", length=255, nullable=false)
     */
    private $revisionableType;

    /**
     * @var int
     *
     * @ORM\Column(name="revisionable_id", type="integer", nullable=false)
     */
    private $revisionableId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=255, nullable=false)
     */
    private $key;

    /**
     * @var string|null
     *
     * @ORM\Column(name="old_value", type="text", length=16777215, nullable=true)
     */
    private $oldValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="new_value", type="text", length=16777215, nullable=true)
     */
    private $newValue;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


}
