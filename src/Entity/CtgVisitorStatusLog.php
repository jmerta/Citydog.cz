<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgVisitorStatusLog
 *
 * @ORM\Table(name="ctg_visitor_status_log")
 * @ORM\Entity
 */
class CtgVisitorStatusLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_visitor", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idVisitor;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=false)
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="status_till", type="datetime", nullable=true, options={"default"="2119-01-01 00:00:00"})
     */
    private $statusTill = '2119-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=false)
     */
    private $note;

    /**
     * @var int
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idComment;


}
