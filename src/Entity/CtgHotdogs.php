<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgHotdogs
 *
 * @ORM\Table(name="ctg_hotdogs", indexes={@ORM\Index(name="post_date_2", columns={"post_date", "post_sort_weight"}), @ORM\Index(name="sort_date", columns={"sort_date"})})
 * @ORM\Entity
 */
class CtgHotdogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPost;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="page_title", type="string", length=255, nullable=false)
     */
    private $pageTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=false)
     */
    private $keywords = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="post_title", type="string", length=255, nullable=false)
     */
    private $postTitle = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="post_date", type="date", nullable=false, options={"default"="0000-00-00"})
     */
    private $postDate = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sort_date", type="date", nullable=false, options={"default"="0000-00-00"})
     */
    private $sortDate = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="post_image", type="string", length=50, nullable=false)
     */
    private $postImage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="post_small_image", type="string", length=50, nullable=false)
     */
    private $postSmallImage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="post_image_readmore", type="string", length=50, nullable=false)
     */
    private $postImageReadmore;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_summary", type="text", length=65535, nullable=true)
     */
    private $postSummary;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_description", type="text", length=65535, nullable=true)
     */
    private $postDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=true)
     */
    private $text;

    /**
     * @var int
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false, options={"comment"="deprecated!"})
     */
    private $idCategory;

    /**
     * @var array|null
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=true)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="feed_type", type="integer", nullable=false, options={"default"="1"})
     */
    private $feedType = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="approved_comments", type="integer", nullable=false)
     */
    private $approvedComments = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=false)
     */
    private $idRubric = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="post_site", type="string", length=100, nullable=false)
     */
    private $postSite;

    /**
     * @var string
     *
     * @ORM\Column(name="post_tovar_url", type="string", length=200, nullable=false)
     */
    private $postTovarUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="post_seller", type="string", length=100, nullable=false)
     */
    private $postSeller;

    /**
     * @var string
     *
     * @ORM\Column(name="post_seller_data", type="string", length=150, nullable=false)
     */
    private $postSellerData;

    /**
     * @var string
     *
     * @ORM\Column(name="post_seller_address", type="string", length=150, nullable=false)
     */
    private $postSellerAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="post_price", type="string", length=50, nullable=false)
     */
    private $postPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="post_sort_weight", type="integer", nullable=false, options={"default"="1000"})
     */
    private $postSortWeight = '1000';

    /**
     * @var string
     *
     * @ORM\Column(name="promo_option", type="string", length=20, nullable=false, options={"default"="none"})
     */
    private $promoOption = 'none';

    /**
     * @var string
     *
     * @ORM\Column(name="post_site_text", type="string", length=255, nullable=false)
     */
    private $postSiteText;

    /**
     * @var bool
     *
     * @ORM\Column(name="our_choise", type="boolean", nullable=false)
     */
    private $ourChoise;

    /**
     * @var string
     *
     * @ORM\Column(name="post_ourchoise_image", type="string", length=50, nullable=false)
     */
    private $postOurchoiseImage;

    /**
     * @var string
     *
     * @ORM\Column(name="post_ourchoise_title", type="string", length=150, nullable=false)
     */
    private $postOurchoiseTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=100, nullable=false)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="text", length=65535, nullable=false)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=100, nullable=false)
     */
    private $tags;


}
