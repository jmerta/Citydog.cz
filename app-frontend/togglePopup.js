function togglePopup() {
    document.querySelector("#supportPopup").classList.toggle('active');
}
if (document.querySelector("#closePopup")){
    document.querySelector("#closePopup").addEventListener('click', (e) => {
    e.preventDefault();
    togglePopup()
});
}
if (document.querySelector("#openPopup")) {
    document.querySelector("#openPopup").addEventListener('click', (e) => {
        e.preventDefault();
        togglePopup()
    });
}
if (document.querySelector("#openPopupMobile")) {
    document.querySelector("#openPopupMobile").addEventListener('click', (e) => {
        e.preventDefault();
        togglePopup()
    });
}